" Vim syntax file
" Language:     SPWN
" Maintainer:   verticallity <verticallity.name@gmail.com>
" Last Change:  2021-01-09
" This file is a modification of the builtin Rust syntax file,
" which is maintained by
" Patrick Walton <pcwalton@mozilla.com>
" Ben Blum <bblum@cs.cmu.edu>
" Chris Morgan <me@chrismorgan.info>

if version < 600
	syntax clear
elseif exists("b:current_syntax")
	finish
endif



syn match spwnType "@\w\(\w\)*"
syn match spwnBuiltin "\$"

syn keyword   spwnConditional if else switch
syn keyword   spwnRepeat      for for_loop while_loop
syn keyword   spwnTypedecl    type nextgroup=spwnType skipwhite skipempty
"syn keyword   spwnOperator    as
syn keyword   spwnConvert     as

syn keyword   spwnKeyword     break continue return
syn keyword   spwnKeyword     in impl let
syn keyword   spwnImport      extract import nextgroup=spwnIdentifier,spwnImport skipwhite skipempty

syn match     spwnIdentifier  contains=spwnIdentifierPrime "\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*" display contained
syn match     spwnMacroName   "\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\)\%([^[:cntrl:][:punct:][:space:]]\|_\)*" display contained

syn region    spwnBoxPlacement matchgroup=spwnBoxPlacementParens start="(" end=")" contains=TOP contained
syn region    spwnBoxPlacementBalance start="(" end=")" containedin=spwnBoxPlacement transparent
syn region    spwnBoxPlacementBalance start="\[" end="\]" containedin=spwnBoxPlacement transparent

syn keyword   spwnSelf        self
syn keyword   spwnBoolean     true false

syn match     spwnModPath     "\w\(\w\)*::[^<]"he=e-3,me=e-3
syn match     spwnModPathSep  "::"

syn match     spwnMacroCall    "\w\(\w\)*("he=e-1,me=e-1

syn match     spwnOperator     display "\%(+\|-\|/\|*\|=\|\^\|&\||\|!\|>\|<\|%\)=\?"
syn match     spwnOperator     display "&&\|||"
syn match     spwnQuestionMark display "?"
syn match     spwnArrow "->"

syn match     spwnTriggerFunc '\w*!'

syn match     spwnEscapeError   display contained /\\./
syn match     spwnEscape        display contained /\\\([nrt0\\'"]\|x\x\{2}\)/
syn match     spwnEscapeUnicode display contained /\\u{\x\{1,6}}/
syn match     spwnStringContinuation display contained /\\\n\s*/
syn region    spwnString      start=+b"+ skip=+\\\\\|\\"+ end=+"+ contains=spwnEscape,spwnEscapeError,spwnStringContinuation
syn region    spwnString      start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=spwnEscape,spwnEscapeUnicode,spwnEscapeError,spwnStringContinuation,@Spell
syn region    spwnString      start='b\?r\z(#*\)"' end='"\z1' contains=@Spell

syn region    spwnAttribute   start="#!\?\[" end="\]" contains=spwnString,spwnDerive,spwnCommentLine,spwnCommentBlock,spwnCommentLineDocError,spwnCommentBlockDocError
" syn region    spwnDerive      start="derive(" end=")" contained contains=spwnDeriveTrait

" Number literals
syn match     spwnDecNumber   display "\<[0-9][0-9_]*\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     spwnHexNumber   display "\<0x[a-fA-F0-9_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     spwnOctNumber   display "\<0o[0-7_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     spwnBinNumber   display "\<0b[01_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     spwnNumberType  display "\(\<\d\(\d\)*\)\@<=[gcib]"
syn match     spwnNumberType  display "\(?\)\@<=[gcib]"

" Special case for numbers of the form "1." which are float literals, unless followed by
" an identifier, which makes them integer literals with a method call or field access,
" or by another ".", which makes them integer literals followed by the ".." token.
" (This must go first so the others take precedence.)
syn match     spwnFloat       display "\<[0-9][0-9_]*\.\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\|\.\)\@!"
" To mark a number as a normal float, it must have at least one of the three things integral values don't have:
" a decimal point and more numbers; an exponent; and a type suffix.
syn match     spwnFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\%([eE][+-]\=[0-9_]\+\)\=\(f32\|f64\)\="
syn match     spwnFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\=\%([eE][+-]\=[0-9_]\+\)\(f32\|f64\)\="
syn match     spwnFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\=\%([eE][+-]\=[0-9_]\+\)\=\(f32\|f64\)"

"spwnLifetime must appear before spwnCharacter, or chars will get the lifetime highlighting
syn match   spwnCharacterInvalid   display contained /b\?'\zs[\n\r\t']\ze'/

" The groups negated here add up to 0-255 but nothing else (they do not seem to go beyond ASCII).
syn match   spwnCharacterInvalidUnicode   display contained /b'\zs[^[:cntrl:][:graph:][:alnum:][:space:]]\ze'/
syn match   spwnCharacter   /b'\([^\\]\|\\\(.\|x\x\{2}\)\)'/ contains=spwnEscape,spwnEscapeError,spwnCharacterInvalid,spwnCharacterInvalidUnicode
syn match   spwnCharacter   /'\([^\\]\|\\\(.\|x\x\{2}\|u{\x\{1,6}}\)\)'/ contains=spwnEscape,spwnEscapeUnicode,spwnEscapeError,spwnCharacterInvalid

syn match spwnShebang /\%^#![^[].*/
syn region spwnCommentLine                                                  start="//"                      end="$"   contains=spwnTodo,@Spell
syn region spwnCommentLineDoc                                               start="//\%(//\@!\|!\)"         end="$"   contains=spwnTodo,@Spell
syn region spwnCommentLineDocError                                          start="//\%(//\@!\|!\)"         end="$"   contains=spwnTodo,@Spell contained
syn region spwnCommentBlock             matchgroup=spwnCommentBlock         start="/\*\%(!\|\*[*/]\@!\)\@!" end="\*/" contains=spwnTodo,spwnCommentBlockNest,@Spell
syn region spwnCommentBlockDoc          matchgroup=spwnCommentBlockDoc      start="/\*\%(!\|\*[*/]\@!\)"    end="\*/" contains=spwnTodo,spwnCommentBlockDocNest,@Spell
syn region spwnCommentBlockDocError     matchgroup=spwnCommentBlockDocError start="/\*\%(!\|\*[*/]\@!\)"    end="\*/" contains=spwnTodo,spwnCommentBlockDocNestError,@Spell contained
syn region spwnCommentBlockNest         matchgroup=spwnCommentBlock         start="/\*"                     end="\*/" contains=spwnTodo,spwnCommentBlockNest,@Spell contained transparent
syn region spwnCommentBlockDocNest      matchgroup=spwnCommentBlockDoc      start="/\*"                     end="\*/" contains=spwnTodo,spwnCommentBlockDocNest,@Spell contained transparent
syn region spwnCommentBlockDocNestError matchgroup=spwnCommentBlockDocError start="/\*"                     end="\*/" contains=spwnTodo,spwnCommentBlockDocNestError,@Spell contained transparent
" FIXME: this is a really ugly and not fully correct implementation. Most
" importantly, a case like ``/* */*`` should have the final ``*`` not being in
" a comment, but in practice at present it leaves comments open two levels
" deep. But as long as you stay away from that particular case, I *believe*
" the highlighting is correct. Due to the way Vim's syntax engine works
" (greedy for start matches, unlike Rust's tokeniser which is searching for
" the earliest-starting match, start or end), I believe this cannot be solved.
" Oh you who would fix it, don't bother with things like duplicating the Block
" rules and putting ``\*\@<!`` at the start of them; it makes it worse, as
" then you must deal with cases like ``/*/**/*/``. And don't try making it
" worse with ``\%(/\@<!\*\)\@<!``, either...

syn keyword spwnTodo contained TODO FIXME XXX NB NOTE

syn region spwnFoldBraces start="{" end="}" transparent fold

" links
hi def link spwnDecNumber       spwnNumber
hi def link spwnHexNumber       spwnNumber
hi def link spwnOctNumber       spwnNumber
hi def link spwnBinNumber       spwnNumber
hi def link spwnIdentifierPrime spwnIdentifier

hi def link spwnImport        PreProc
hi def link spwnBuiltin       Special
hi def link spwnArrow         Label
hi def link spwnConvert       Keyword

hi def link spwnEscape        Special
hi def link spwnEscapeUnicode spwnEscape
hi def link spwnEscapeError   Error
hi def link spwnStringContinuation Special
hi def link spwnString        String
hi def link spwnCharacterInvalid Error
hi def link spwnCharacterInvalidUnicode spwnCharacterInvalid
hi def link spwnCharacter     Character
hi def link spwnNumber        Number
hi def link spwnBoolean       Boolean
" hi def link spwnConstant      Constant
hi def link spwnSelf          Constant
hi def link spwnFloat         Float
hi def link spwnOperator      Operator
hi def link spwnKeyword       Keyword
hi def link spwnTypedecl      Keyword
hi def link spwnRepeat        Conditional
hi def link spwnConditional   Conditional
hi def link spwnIdentifier    Identifier
hi def link spwnModPath       Include
hi def link spwnModPathSep    Delimiter
hi def link spwnMacroName     Macro
hi def link spwnMacroCall     Macro
hi def link spwnShebang       Comment
hi def link spwnCommentLine   Comment
hi def link spwnCommentLineDoc SpecialComment
hi def link spwnCommentLineDocError Error
hi def link spwnCommentBlock  spwnCommentLine
hi def link spwnCommentBlockDoc spwnCommentLineDoc
hi def link spwnCommentBlockDocError Error
hi def link spwnTriggerFunc   Function
hi def link spwnType          Type
hi def link spwnNumberType    Type
hi def link spwnTodo          Todo
hi def link spwnAttribute     PreProc
hi def link spwnDerive        PreProc
hi def link spwnLabel         Label
hi def link spwnBoxPlacementParens Delimiter
hi def link spwnQuestionMark  Special

syn sync minlines=200
syn sync maxlines=500

let b:current_syntax = "spwn"
