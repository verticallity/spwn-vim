# spwn-vim

vim setup for SPWN, a Geometry Dash trigger language. this may grow to a full SPWN mode, but for now is solely syntax highlighting.


## Installing

with vim-plug, between your `plug#begin` and `plug#end` calls,
```vim
Plug 'https://gitlab.com/verticallity/spwn-vim'
```

or manually, add all files to their corresponding locations in your vim folder (`syntax/spwn.vim` goes to `$HOME/.vim/syntax/spwn.vim`, etc.)

if syntax highlighting does not appear, ensure your vimrc contains `syntax on` and `filetype on` or equivalents
